@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">
            List Leave
        </div>
        <div class="row card-body">
            @if(!empty(session()->get( 'successMsg' )))
                <div class="col-md-12 alert alert-info" role="alert">
                    <stron>Success !!</stron>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="form-group">
                <a type="button" href="{{ route('add-attendance') }}" class="btn btn-primary">Add New</a>
            </div>
            <div class="col-md-12">
                <table id="table-data" class="table table-bordered yajra-datatable">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Absent From</th>
                        <th>Absent To</th>
                        <th>CutOff</th>
                        <th>Attendances</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        window.onload = function () {
            var table = $('#table-data').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('attendances') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'absent_from', name: 'absent_from'},
                    {data: 'absent_to', name: 'absent_to'},
                    {data: 'cutoff', name: 'cutoff'},
                    {data: 'attachment', name: 'attachment'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });

        };
        {{--$(document).ready(function () {--}}
            {{--var table = $('#table-data').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: "{{ route('get.attendances') }}",--}}
                {{--columns: [--}}
                    {{--{data: 'DT_RowIndex', name: 'DT_RowIndex'},--}}
                    {{--{data: 'absent_from', name: 'absent_from'},--}}
                    {{--{data: 'absent_to', name: 'absent_to'},--}}
                    {{--{data: 'cutoff', name: 'cutoff'},--}}
                    {{--{data: 'attachment', name: 'attachment'},--}}
                    {{--{--}}
                        {{--data: 'action',--}}
                        {{--name: 'action',--}}
                        {{--orderable: true,--}}
                        {{--searchable: true--}}
                    {{--},--}}
                {{--]--}}
            {{--});--}}

        {{--});--}}
    </script>
@endsection
