<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/checkin', 'LeavesController@index')->name('checkin');
    Route::post('/clookIn', 'LeavesController@clookIN')->name('add-checkin');
    Route::post('/clookOut', 'LeavesController@clookOUT')->name('add-checkout');

    Route::get('/attendances', 'AttendancesController@index')->name('attendances');
    Route::get('/get-attendances', 'AttendancesController@getDataIndex')->name('get.attendances');
    Route::get('/add-attendances', 'AttendancesController@create')->name('add-attendance');
    Route::post('/store-attendances', 'AttendancesController@store')->name('store.attendances');

});