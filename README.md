# absensi

Dibagi menjadi 2 folder 
1. Absensi
	Berisi source code applikasi,
	- Clone applikasi via git di https://gitlab.com/tri.s48/absensi.git
	- Buat file .env, isi file sama dengan file .env.example
	- Jalankan perintan composer update
	- Login dengan akun susilotri@gmail.com, pass welcome123 atau bisa klik menu register di pojok kanan atas

2. Database
	Berisi file export dari mysql
	- silahkan import file tersebut menggunakan phpmyadmin.
