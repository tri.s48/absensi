@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">
            CheckIn / CheckOut
        </div>
        <div class="row card-body">
           <input type="text" id="leave_id" value="{{ $data ? $data->id : '' }}" class="form-control" readonly hidden>
           <div class="col-sm-5" style="text-align: center">
               <span style="display: block; text-align: center; font-size: 20px; font-weight: bold; color: green">CLOOK IN</span>
               <span id="time_in" style="display: block; text-align: center; font-size: 20px; font-weight: bold; color: black">{{$data ? $data->check_in : '00:00'}}</span>
           </div>
           <div class="col-sm-2" style="text-align: center">
               @if(!empty($data))
                   @if($data->check_out == null)
                       <button id="button_out" type="button" class="btn btn-danger" onclick="clockOUT()" style="width: 75px; border-radius: 50%;  height: 75px; font-weight: bold">OUT</button>
                   @else
                       <button disabled id="button_in" type="button" class="btn btn-success" onclick="clockIN()" style="width: 75px; border-radius: 50%;  height: 75px; font-weight: bold">IN</button>
                   @endif
               @else
                   <button id="button_in" type="button" class="btn btn-success" onclick="clockIN()" style="width: 75px; border-radius: 50%;  height: 75px; font-weight: bold">IN</button>
                   <button id="button_out" type="button" class="btn btn-danger" onclick="clockOUT()" style="display: none;width: 75px; border-radius: 50%;  height: 75px; font-weight: bold">OUT</button>
               @endif
           </div>
           <div class="col-sm-5" style="text-align: center">
               <span style="display: block; text-align: center; font-size: 20px; font-weight: bold; color: red">CLOOK OUT</span>
               <span id="time_out" style="display: block; text-align: center; font-size: 20px; font-weight: bold; color: black">{{$data ? $data->check_out ? : '00:00': '00:00'}}</span>
           </div>
        </div>
    </div>
    
    <script>
        function formatDate(){
            const date = new Date();
            var dateString =
                date.getUTCFullYear() + "-" +
                ("0" + (date.getMonth()+1)).slice(-2) + "-" +
                ("0" + date.getDate()).slice(-2) + " " +
                ("0" + date.getHours()).slice(-2) + ":" +
                ("0" + date.getMinutes()).slice(-2) + ":" +
                ("0" + date.getSeconds()).slice(-2);
            return dateString;
        }
        function clockIN() {
            var dateString = formatDate();
            $.ajax({
                url: "{{ route('add-checkin') }}",
                method: 'post',
                data : {
                    "_token": "{{ csrf_token() }}",
                    'check_in' : dateString,
                },
                success: function (data) {
                    $('#leave_id').val(data.id);
                    $('span#time_in').html(data.check_in);
                    $('#button_in').hide();
                    $('#button_out').show();
                }
            })
        }
        function clockOUT() {
            var dateString = formatDate();
            $.ajax({
                url: "{{ route('add-checkout') }}",
                method: 'post',
                data : {
                    "_token": "{{ csrf_token() }}",
                    'id' : $('#leave_id').val(),
                    'check_out' : dateString,
                },
                success: function (data) {
                    $('span#time_out').html(dateString);
                    $('#button_in').show().attr('disabled', true);
                    $('#button_out').hide();
                }
            })

        }
    </script>
@endsection
