<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leaves extends Model
{
    protected $table = 'leaves';
    protected $fillable = [
        'user_id',
        'check_in',
        'check_out',
    ];
}
