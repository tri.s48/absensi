<?php

namespace App\Http\Controllers;

use App\Attendances;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;

class AttendancesController extends Controller
{
    public function index(Request $request){
        return view('pages.attendances.index');
    }

    public function getDataIndex(){
        $id = Auth::user()->id;
        $data = Attendances::find($id)->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create(){

        return view('pages.attendances.create');
    }
    public function store(Request $request){
        $data = new Attendances();
        $data->user_id = Auth::user()->id;
        $data->absent_from = $request->absent_from;
        $data->absent_to = $request->absent_to;
        $data->cutoff = $request->cutoff;
        if(!is_dir('/img')){
            mkdir('/img', 0777, TRUE);
        }
        $file = $request->file('attachment');
        $nameImage = $request->file('attachment')->getClientOriginalName();
        $oriPath = public_path() . '/img' . $nameImage;
        Image::make($file)->save($oriPath);
        $data->attachment = $nameImage;
        $data->save();

        return Redirect::route('attendances')->with(['successMsg' => true]);
    }
}
