<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-header">ABSENSI</li>
        <li class="nav-item">
            <a href="{{ route('checkin') }}" class="nav-link">
                <i class="nav-icon fas fa-clock"></i>
                <p>
                    Check in
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('attendances') }}" class="nav-link">
                <i class="nav-icon fas fa-comment-alt"></i>
                <p>
                    E-Leave
                </p>
            </a>
        </li>
        <li class="nav-header">SETTING</li>
        <li class="nav-item">
            <a href="pages/calendar.html" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    User
                </p>
            </a>
        </li>
    </ul>
</nav>