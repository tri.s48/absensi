@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">
            Add Leave
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('store.attendances') }}"  enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Absent From</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="absent_from" id="absent_from" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Absent To</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="absent_to" id="absent_to" placeholder="" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Cut Off</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="cutoff" id="cutoff" required>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Attachment</label>
                    <div class="col-sm-10">
                        <input type="file" name="attachment" id="attachment" placeholder="Password" required>
                    </div>
                </div>
                <button type="submit" id="submit" class="btn btn-success waves-effect waves-light" style="margin-left:5px; float: right;">
                    Submit
                </button>
            </form>
        </div>
    </div>

    <script>

    </script>
@endsection
