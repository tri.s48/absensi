<?php

namespace App\Http\Controllers;

use App\Leaves;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;

class LeavesController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::user()->id;
        $data = Leaves::find($id);
        if ($data)
            $data->whereDate('created_at', Carbon::today())->first();
        return view('pages.checkin', compact( 'data'));
    }

    public function clookIN(Request $request){
        $data = Leaves::create([
            'user_id' => Auth::user()->id,
            'check_in' => date( 'Y-m-d H:i:s', strtotime($request->check_in)),
        ]);

        return $data;
    }

    public function clookOUT(Request $request){
        $data = Leaves::where('id', $request->id)->update([
           'check_out' => date('Y-m-d H:i:s', strtotime($request->check_out))
        ]);
        return $data;
    }
}
